package org.firstinspires.ftc.teamcode.Walbots7175.internal;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.AutonomousMode;


/**
 * The class AutonomousMode_Blue_Back implements AutonomousMode and is registered so that it will
 * show up on the phone. It is defined to be running when we are in the blue alliance on the back
 * starting position. All actions are done by it's super class AutonomousMode.
 *
 * @see org.firstinspires.ftc.teamcode.AutonomousMode
 */
@Autonomous(name = "Blue Alliance Back", group = "Autonomous")
public class AutonomousMode_Blue_Back extends AutonomousMode
{
    /**
     * The AutonomousMode implementation AutonomousMode_Blue_Back is defined to be in the blue
     * alliance so this function will always return true.
     *
     * @return Always true for being in the blue alliance.
     */
    @Override
    public boolean isBlueAlliance()
    {
        return true;
    }

    /**
     * The AutonomousMode implementation AutonomousMode_Blue_Back is defined to be in the back place
     * so this function will always return false.
     *
     * @return Always false for being in the back place.
     */
    @Override
    public boolean isFrontPlace()
    {
        return false;
    }
}
